# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright (C) 2019 EVBox B.V
# Copyright (C) 2019 Olliver Schinagl <oliver@schinagl.nl>

ARG ALPINE_VERSION="3.15"
ARG TARGET_ARCH="library"

FROM index.docker.io/${TARGET_ARCH}/alpine:${ALPINE_VERSION}

LABEL maintainer="Olliver Schinagl <oliver@schinagl.nl>"

EXPOSE 20/tcp
EXPOSE 21/tcp
EXPOSE 53/udp
EXPOSE 67/udp
EXPOSE 68/udp
EXPOSE 69/udp
EXPOSE 80/tcp
EXPOSE 111/tcp
EXPOSE 111/udp
EXPOSE 123/tcp
EXPOSE 123/udp
EXPOSE 514/udp
EXPOSE 989/tcp
EXPOSE 989/udp
EXPOSE 990/tcp
EXPOSE 990/udp
EXPOSE 2049/tcp
EXPOSE 2049/udp
EXPOSE 5201/tcp
EXPOSE 5201/udp

VOLUME "/var/log/"

RUN apk add --no-cache \
        busybox-extras \
        dnsmasq \
        dumb-init \
        imx_usb_loader \
        iperf3 \
        iptables \
        libusb \
        logrotate \
        nfs-utils \
        openntpd \
        openssl \
        rsyslog \
        tftp-hpa \
        vsftpd \
    && \
    rm -f -r '/var/cache/apk/'* && \
    mkdir -p '/var/lib/dhcp/' && \
    touch '/var/lib/dhcp/dhcpd.leases' && \
    mkdir -p \
          '/var/lib/nfs/rpc_pipefs' \
          '/var/lib/nfs/v4recovery' \
    && \
    ( \
        echo 'nfsd  /proc/fs/nfsd   nfsd    defaults        0       0' \
        echo 'rpc_pipefs    /var/lib/nfs/rpc_pipefs rpc_pipefs      defaults        0       0' \
    ) >> '/etc/fstab'

COPY "./containerfiles/buildenv_check.sh" "/test/buildenv_check.sh"
COPY "./containerfiles/dnsmasq/*" "/etc/dnsmasq.d/"
COPY "./containerfiles/exports" "/etc/exports"
COPY "./containerfiles/init.sh" "/init"
COPY "./containerfiles/logrotate.conf" "/etc/logrotate.conf"
COPY "./containerfiles/nfsd.sh" "/usr/bin/nfsd.sh"
COPY "./containerfiles/ntpd.conf" "/etc/ntpd.conf"
COPY "./containerfiles/rsyslog/*" "/etc/rsyslog.d/"
COPY "./containerfiles/vsftpd/*" "/etc/vsftpd/"

RUN chmod 'go-r' '/etc/logrotate.conf'

CMD [ "/init" ]
