# Bootstrap
When an embedded board is completely empty, a method is required to load it up
with software to be functional. To accomplish this a few basic components are
needed. The bootstrapping server requires a dedicated Ethernet device and will
use the default address **192.168.163.1/24**.

## Wrapper
Included in this repository is script called 'docker_bootstrap.sh'. This script
can be used to simplify building the container, running docker with all its
arguments. The container technology used throughout this repository is `docker`
however, any other container tech should be usable.

### Prerequisites
To ensure that the included 'docker_bootstrap.sh' script can run without
problems the script will perform a self-test at start. If there are missing
dependencies, the script will print an error and inform about what was missing.

### Usage
To simplify things more, a symlink can be put into the local path. For example
in *~/bin* or */usr/local/bin*. In the following example the script is
optionally renamed. Care is to be taken however when renaming to avoid conflicts
with other files.
```console
$ ln -s /path/to/docker_bootstrap.sh ~/bin/docker_bootstrap.sh
```

Then, the script can be called from the directory containing the binary files to
be hosted. For example using the hosts second Ethernet device *eth2*:
```console
$ cd /directory/holding/binary_output/
$ docker_bootstrap.sh -i eth2
```

Alternatively the *-b* parameter can be passed to the script containing the
directory to be served.
```console
$ docker_bootstrap.sh -i eth2 -b /directory/holding/binary_output/
```

Note, that files to be served by TFTP need to be in a sub-directory called
*tftpboot*, e.g. '/directory/holding/binary_output/tftpboot/'. Files to be
served via HTTP need to be in a sub-directory called *www*, e.g.
'/directory/holding/binary_output/www/'. Files to be served via NFS need
to be in a sub-directory called *nfs*, eg
`/directory/holding/binary_output/nfs/`. Files to be served by `imx_usb_loader`
(aka serial downloader) need to be in a sub-directory called *imxboot*, e.g.
'/directory/holding/binary_output/imxboot/'.
To match the firmware with the device used, the VID and the PID of the usb device
is needed for the filename. Best is to use a symlink for this, e.g.
` ` `sh
ln -s "u-boot.imx" "0x15a2:0x0080"
ln -s "u-boot-spl.bin" "0x15a2:0x107f"
` ` `
To find those, use `lsusb`.
The optional configuration files for imxboot need to be in a sub-directory
called *configs*, e.g.
`/directory/holding/binary_output/imxboot/configs/`.

An ftp server will serve files in `/directory/holding/binary_output/ftp` in
both **ftp** and **ftps** protocols on port 21, and **implicit ftps** on port
`990`. If a specific configuration file is desired, this is to be stored in
(after remapping) `/etc/vsftpd/vsftpd_implicit.conf`, if this file does not
exist, the regular `vsftpd.conf` file is copied and implicit ftps is enabled on
port `990`. When no private key/certificate is supplied, a self-signed pair
will be generated.

> __WARNING:__ The FTP server is setup in a very insecure way by default.
> While it supports both anonymous (without password) and user-based logins,
> any user can login with any password!! The goal here is not to setup a
> secure FTP server. Hardening IS possible, by changing the `vsftpd/vsftpd.pam`
> module for vsftpd.

If the 'ftp' or 'www' is not a directory, then the 'tftpboot' directory will be used
to serve the files instead.


### Volumes
By default, the `docker_bootstrap.sh` script prefers bind mounts to mount a
supplied directory, this as this makes invocation easy, just point to a
location holding the correct files.

However, it is also quite easy and feasible to create a shared volume, so that
it is possible to have a container running bootstrap, be supplied with the
needed files from a second container. To create, and seed the volume, a
temporary docker container is needed however. In this example alpine is used,
but anything can be used that has the `mkdir` binary.

In this example the `tftpboot` and `www` directories are created, but this can
be expanded with whatever directories are needed.
```console
docker run --rm --interactive --tty --volume my_volume:/volume alpine:latest mkdir /volume/{tftpboot,www}
```

To verify everything worked, the contents of the directory can be listed.
```console
docker run --rm --interactive --tty --volume my_volume:/volume alpine:latest ls -l /volume/
imxboot/ tftpboot/ www/
```

> __Note:__ The directories are created under `/volume` due to the fact that
> the voleume is mounted to this location.


### NFS support
The *-n* parameter can also be used to the script containing the directory
to be served over NFS.
```console
$ docker_bootstrap.sh -i eth2 -n directory/containing/nfs
```

If the 'nfs' is not a directory, then the 'tftpboot' directory will be used
to serve the files instead.

Note that this directory can not be inside an ecryptfs directory, needs to be a
relative path from the start directory (due to docker volumes), and this NFS
share will only available on the bootstraps default network.

### Options
The wrapper script can pass additional environment variables or mount points
into the container if needed. While using the option flags is straight forward,
passing more then one of these via environment variables can be tricky and
should be avoided.

Simply put however, any additional arguments will not be pre-processed and are
basically passed straight through to docker.

## Building
To create the bootstrap environment, simply build the local docker container,
or use the one from the registry.
```console
$ docker build --rm -t bootstrap:latest .
```
Take note of the used tag, 'bootstrap:latest' as this will be needed when
running the container explained below. Also note that any name can be chosen
as long as this is done consistently.

## Running
Running the container has a few notes to be very careful about. First, the
container requires access to configure the network hardware and do network
broadcast. As such, *privileged* mode is required.

Further more, the container needs access to a physical network port. On laptops
this can be realized by using the fixed Ethernet for the container and WiFi for
normal network connectivity, if so required. Alternatively a second Ethernet
device can be added to the host also.

Finally, the files to be served via the tftp server. This is best accomplished
by being in the directory that contains the files to be hosted.

For example using the hosts second Ethernet device, *eth2* would look something
like this.
```console
$ cd /directory/holding/binary_output/
$ docker run \
    --network host \
    --privileged
    --rm \
    -e DHCPD_IFACE="eth2" \
    -i \
    -t \
    -v "$(pwd):/bootstrap"
    bootstrap:latest
```

This container can also be run in the background on a dedicated server. This is
purposely left as an exercise to the reader.


### Testing container
During development, it is often desired to run the container to see how it
behaves. Often in those scenario's, a dedicated network adapter is not
available. Using tun/tap interfaces however can easily work around this.

As root, create a virtual *tap* interface to be used for `DHCPD_IFACE`
```sh
ip tuntap add mode tap tap0
```

> __Note:__ While doing very complex things, like creating bridges with this
> tap interface etc are obviously possible, quite out of scope for this
> document.


## Logging
By default, the init script tails to the container console `/var/log/messages`.
If logs are to be kept or other files are to be logged in different means,
a volume or volume mount can be created for `/var/log/` and handled externally.

The container uses by default an ephemeral volume for `/var/log/`. This volume
can be volume-mounted if logs are desired to be kept persisted.

To add additional rsyslog configuration, `/etc/rsyslog.d` can be mounted to add
additional configurations.

> __Note:__ Care needs to be taken as the remote logging configuration file
> is installed in this location as well.

Volume mounting has not native support in the script, but can be done very
easily by adding this to the `opt_docker_args` variable.
```sh
OPT_DOCKER_ARGS="--volume 'logging_volume:/var/log' ${OPT_DOCKER_ARGS:-}"
```


### Remote logging
The default configuration of bootstrap will accept remote logs on UDP port 514
using the standard syslog protocol. By default, that means the log will show:
* Date
* IP or hostname
* Message from the client

Thus to manually log something from a target for example:
```console
user@TestHost01 $ logger -t $(hostname) "Hello World!"
```

will print
```console
2022-02-22T18:31:19+00:00 172.17.0.2 TestHost01: Hello World!
```

Any system logger that supports the syslog format should be able to use this.


## Debugging
Sometimes things do not go as expected and then it helps to be able to
investigate things.

### Running container
For a running container, `docker exec`
can be used. First, obtain the name of the container. `docker container ls`
will yield the name in the last column. Alternatively the *--name* parameter can
be supplied to docker run. Then use this name to start a program in the running
container.
```console
$ docker exec -it bootstrap-container /bin/sh
```
Will start a shell inside the container named 'bootstrap-container'. The *-it*
parameters are used to execute an *interactive* command on a *terminal* and is
only needed for a shell.

### No container
If there is no container started, simply append the command to run to the docker
run command from above. So by appending '/bin/sh' we get a shell inside said
container. No programs will be started up so that will have to be done manually,
for example by executing the commands from */init*.

### Virtual Machine
Supplied is a basic Vagrantfile that can be used with `vagrant up` to create
a Virtual machine. A few manual steps are still required however, such as adding
a second network adapter and running the actual installation (which can only be
done after adding the second adapter). The adding of the network adapter can not
be done automatically due to the fact that it is best to use the pass-through
option here which would be different for every system. Two providers are
supported, libvirt-kvm and virtualbox.

> __Note__: in the scripts subdirectory a script 'install.sh' is found. This script
> can be used to install this repository to a local system. It currently only
> works with Standard Debian. Warning, the script will modify the way networking
> behaves and without a second network interface, tasks that require a internet
> connection (to update etc) will not work without this.
