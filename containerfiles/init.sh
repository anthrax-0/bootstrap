#!/usr/bin/dumb-init /bin/sh
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright (C) 2019 EVBox B.V.
# Copyright (C) 2019 Olliver Schinagl <oliver@schinagl.nl>

set -eu

FTP_ROOT='/bootstrap/ftp/'
IP_ADDR="${IP_ADDR:-192.168.163.1}"
IMX_CONF="${IMX_CONF:-/etc/imx-loader.d/}"
IMX_ROOT='/bootstrap/imxboot/'
NFS_ROOT="${SHARED_DIRECTORY:-/bootstrap/nfs/}"
TFTP_ROOT='/bootstrap/tftpboot/'
WWW_ROOT='/bootstrap/www/'
ssl_crt='/etc/bootstrap.crt'
ssl_key='/etc/bootstrap.key'


_msg()
{
	_level="${1:?Missing argument to function}"
	shift

	if [ "${#}" -le 0 ]; then
		echo "${_level}: No content for this message ..."
		return
	fi

	echo "${_level}: ${*}"
}

e_err()
{
	_msg 'err' "${*}" >&2
}

e_warn()
{
	_msg 'warning' "${*}"
}

e_notice()
{
	_msg 'notice' "${*}"
}

if ! ip link set "${DHCPD_IFACE:?}" up; then
	e_err "Unable to bring up network device '${DHCPD_IFACE}'."
	exit 1
fi

if ! ip address replace "${IP_ADDR}/24" dev "${DHCPD_IFACE:?}"; then
	e_err "Unable to configure network device '${DHCPD_IFACE}'."
	exit 1
fi

if ! ip route replace "${IP_ADDR%.*}.0/24" dev "${DHCPD_IFACE:?}"; then
	e_err "Unable to configure route for device '${DHCPD_IFACE}'."
	exit 1
fi

if ! rsyslogd; then
	e_warn 'Failed to start syslog, logging may not work.'
fi

sleep 1

if [ -z "${SSL_KEY_PREFIX:-}" ]; then
	if [ ! -f "${ssl_crt:-}" ] && \
	   [ ! -f "${ssl_key:-}" ]; then
		openssl req \
		            -newkey rsa:1024 \
		            -nodes \
		            -keyout "${ssl_key}" \
		            -x509 \
		            -days 365 \
		            -out "${ssl_crt}" \
		            -subj '/C=NL/ST=State/L=City/O=Company Inc./OU=IT/CN=ESBS Bootstrap self-signed cert'
	fi
else
	if [ -f "${SSL_KEY_PREFIX}.key" ]; then
		ssl_key="${SSL_KEY_PREFIX}.key"
	fi
	if [ -f "${SSL_KEY_PREFIX}.crt" ]; then
		ssl_crt="${SSL_KEY_PREFIX}.crt"
	fi
fi

if [ ! -d "${TFTP_ROOT}" ]; then
	e_err "Missing TFTP_ROOT directory '${TFTP_ROOT}', cannot start."
	exit 1
fi

if ! in.tftpd --address "${IP_ADDR}" --listen --secure --verbosity=7 "${TFTP_ROOT}"; then
	e_err 'Failed to start in.tftpd.'
	exit 1
fi

if [ ! -d "${WWW_ROOT}" ]; then
	WWW_ROOT="${TFTP_ROOT}"
fi

if ! httpd -h "${WWW_ROOT}" -p "${IP_ADDR}"; then
	e_warn 'Failed to start httpd server.'
fi

if [ ! -d "${FTP_ROOT}" ]; then
	FTP_ROOT="${WWW_ROOT}"
fi

if grep -q '_root=' '/etc/vsftpd/vsftpd.conf'; then
	sed -i "s|\(^.*_root=\).*$|\1${FTP_ROOT}|g" '/etc/vsftpd/vsftpd.conf'
else
	echo "anon_root=${FTP_ROOT}" >> '/etc/vsftpd/vsftpd.conf'
	echo "local_root=${FTP_ROOT}" >> '/etc/vsftpd/vsftpd.conf'
fi
if grep -q 'background' '/etc/vsftpd/vsftpd.conf'; then
	sed -i 's|^background=.*$|background=YES|g' '/etc/vsftpd/vsftpd.conf'
else
	echo 'background=YES' >> '/etc/vsftpd/vsftpd.conf'
fi
if grep -q 'listen_address=' '/etc/vsftpd/vsftpd'; then
	sed -i "s|^listen_address=.*$|listen_address=${IP_ADDR}|g" '/etc/vsftpd/vsftpd.conf'
else
	echo "listen_address=${IP_ADD:-0.0.0.0}" >> '/etc/vsftpd/vsftpd.conf'
fi
if [ -f "${ssl_crt:-}" ] && \
   [ -f "${ssl_key:-}" ]; then
	if grep -q 'ssl_enable=' '/etc/vsftpd/vsftpd.conf'; then
		sed -i 's|^ssl_enable.*$|ssl_enable=YES|g' '/etc/vsftpd/vsftpd.conf'
	else
		echo 'ssl_enable=YES' >> '/etc/vsftpd/vsftpd.conf'
	fi
	if grep -q 'rsa_cert_file=' '/etc/vsftpd/vsftpd.conf'; then
		sed -i "s|^rsa_cert_file=.*$|rsa_cert_file=${ssl_crt}|g" '/etc/vsftpd/vsftpd.conf'
	else
		echo "rsa_cert_file=${ssl_crt}" >> '/etc/vsftpd/vsftpd.conf'
	fi
	if grep -q 'rsa_private_key_file=' '/etc/vsftpd/vsftpd.conf'; then
		sed -i "s|^rsa_private_key_file=.*$|rsa_private_key_file=${ssl_key}|g" '/etc/vsftpd/vsftpd.conf'
	else
		echo "rsa_private_key_file=${ssl_key}" >> '/etc/vsftpd/vsftpd.conf'
	fi
	if ! grep -q 'require_ssl_reuse=' '/etc/vsftpd/vsftpd.conf'; then
		echo 'require_ssl_reuse=NO' >> '/etc/vsftpd/vsftpd.conf'
	fi

fi
if [ -f '/etc/vsftpd/vsftpd.pam' ]; then
	cp '/etc/vsftpd/vsftpd.pam' '/etc/pam.d/vsftpd'
fi

if ! vsftpd '/etc/vsftpd/vsftpd.conf'; then
	e_warn 'Failed to start vsftpd server.'
fi

if [ -f '/etc/vsftpd/vsftpd_implicit.conf' ]; then
	vsftpd_implicit_conf='/etc/vsftpd/vsftpd_implicit.conf'
else
	vsftpd_implicit_conf='/etc/vsftpd_implicit.conf'

	cp '/etc/vsftpd/vsftpd.conf' "${vsftpd_implicit_conf}"
	if grep -q 'listen_port' "${vsftpd_implicit_conf}"; then
		sed -i 's|^listen_port=.*$|listen_port=990|g' "${vsftpd_implicit_conf}"
	else
		echo 'listen_port=990' >> "${vsftpd_implicit_conf}"
	fi

	if grep -q 'implicit_ssl=' "${vsftpd_implicit_conf}"; then
		sed -i 's|^implicit_ssl=.*$|implicit_ssl=yes|g' "${vsftpd_implicit_conf}"
	else
		echo 'implicit_ssl=yes' >> "${vsftpd_implicit_conf}"
	fi
fi

if ! vsftpd "${vsftpd_implicit_conf:?}"; then
	e_warn 'Failed to start implicit SSL vsftpd server on port 990.'
fi

if ! iperf3 --bind "${IP_ADDR}" --daemon --logfile '/var/log/iperf3.log' --server; then
	e_warn 'Failed to start iperf3 server.'
fi

if [ -n "${NAT_IFACE:-}" ]; then
	echo "Setting up iptables NAT'ing via '${NAT_IFACE}'"
	iptables -A FORWARD -j ACCEPT
	iptables -t nat -A POSTROUTING -o "${NAT_IFACE}" -j MASQUERADE
	echo 'Enabling IP forwarding!'
	sysctl -w net.ipv4.ip_forward=1
fi

sed -i "s|@IP_ADDR@|${IP_ADDR}|g" '/etc/ntpd.conf'
if ! ntpd; then
	e_warn 'Failed to start ntpd server.'
fi

if [ ! -d "${NFS_ROOT:-}" ]; then
	NFS_ROOT="${TFTP_ROOT}"
fi
if [ -d "${NFS_ROOT:-}" ]; then
	PERMITTED="${PERMITTED:-"${IP_ADDR%'.'*}"'.'*}" \
	SHARED_DIRECTORY="${NFS_ROOT}" \
	'/usr/bin/nfsd.sh' &
fi

for _dnsmasqconf in "/etc/dnsmasq.d/"*'.in'; do
	if [ ! -f "${_dnsmasqconf}" ]; then
		continue
	fi
	sed \
	    -e "s|@DHCPD_IFACE@|${DHCPD_IFACE}|g" \
	    -e "s|@IP_PREFIX@|${IP_ADDR%'.'*}|g" \
	    -e "s|@IP_NETMASK@|${IP_NETMASK:-255.255.255.0}|g" \
	    "${_dnsmasqconf}" > "${_dnsmasqconf%%.in}"
done
if ! dnsmasq -8 -; then
	e_err "Failed to start dnsmasq"
	exit 1
fi

if [ -d "${IMX_ROOT}/configs/" ]; then
	IMX_CONF="${IMX_ROOT}/configs/"
fi

# shellcheck disable=SC2021  # Busybox tr is non-posix without classes
IMX_USB_VID_PID="$(sed -n 's|^0x\(.\{4\}\):0x\(.\{4\}\).*|\1:\2|p' "${IMX_CONF}/imx_usb.conf" | \
                   sort -u | \
                   tr '[AZ]' '[az]')"

tail -f '/var/log/messages' '/var/log/iperf3.log' &

chmod 'go-rwx' '/etc/logrotate.conf'

if [ -d "${TFTP_ROOT}" ]; then
	chmod 'a+rX' -R "${TFTP_ROOT}"
fi
if [ -d "${IMX_ROOT}" ]; then
	chmod 'a+rX' -R "${IMX_ROOT}"
fi

while true; do
	if [ -d "${IMX_ROOT}" ]; then
		for _vid_pid in ${IMX_USB_VID_PID:-}; do
			_pid="${_vid_pid#?????}"
			_vid="${_vid_pid%?????}"
			if grep -q "${_vid}" '/sys/bus/usb/devices/'*'/idVendor' &&
			   grep -q "${_pid}" '/sys/bus/usb/devices/'*'/idProduct'; then
				if [ -f "${IMX_ROOT}/0x${_vid}:0x${_pid}" ]; then
				    imx_usb -c "${IMX_CONF}" "${IMX_ROOT}/0x${_vid}:0x${_pid}" || true
				else
				    e_warn "Firmware file '0x${_vid}:0x${_pid}' missing"
				fi
				break 1
			fi
		done
	fi

	sleep 1
	if [ "${_logrotate_delay:=0}" -ge 3600 ]; then
		if ! logrotate '/etc/logrotate.conf'; then
			e_warn 'Logrotation failed.'
		fi
		_logrotate_delay=0
	fi
	_logrotate_delay="$((_logrotate_delay + 1))"
done

exit 0
